GL C/C++ BaseCamp 2022 Task1 Heorhii Mamonov

Variant 12

12. Client/Server application to search directory on remote HOST.
- client has functionality to search directory by server request.
- server will request search by "directory name"
- client has to replay with "processing" each 500ms of searching
- client has to reply with success or failure.
- success replay has to include full path to the directory.


