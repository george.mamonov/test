#include "DirectorySearcher.h"

#include <algorithm>

namespace sys
{
    void DirectorySearcher::search_for_directory(const std::string &directory, SearchResult &result)
    {
        search_for_directory(directory, ROOT_PATH, result);
    }

    void DirectorySearcher::search_for_directory(const std::string &directory, const std::string &where, SearchResult &result)
    {

        clear_queue();
        result.m_ready = 0;
        m_directory_queue_size = 1;
        m_result = &result;
        m_directory_to_search = directory;
        m_directory_queue.push(where);

        start_workers();
    }

    void DirectorySearcher::stop_search()
    {
        m_should_work = 0;
        join_workers();
        clear_queue();
    }

    DirectorySearcher::~DirectorySearcher()
    {
        join_workers();
    }

    void DirectorySearcher::start_workers()
    {
        m_should_work = 1;

        for (int i = 0; i < m_can_stop.size(); i++)
            m_can_stop[i] = 0;

        for (int i = 0; i < m_threads.size(); i++)
        {
            m_threads[i] = std::move(std::thread(&DirectorySearcher::start_search_thread, this, i));
        }
    }

    void DirectorySearcher::join_workers()
    {
        m_should_work = 0;

        for (auto &it : m_threads)
        {
            if (it.joinable())
                it.join();
        }
    }

    void DirectorySearcher::clear_queue()
    {
        m_queue_mutex.lock();
        while (m_directory_queue.size())
        {
            m_directory_queue.pop();
        }
        m_queue_mutex.unlock();
    }

    void DirectorySearcher::start_search_thread(size_t id)
    {
        m_can_stop[id] = 0;

        std::string path;

        while (m_should_work)
        {
            m_queue_mutex.lock();
            if (!m_directory_queue.empty())
            {
                m_can_stop[id] = 0;
                path = std::move(m_directory_queue.front());
                m_directory_queue.pop();
                m_directory_queue_size--;
            }
            else
            {
                m_can_stop[id] = 1;
            }
            m_queue_mutex.unlock();

            if (!m_can_stop[id])
            {
                count++;
                process_dir(path);
            }
            else
            {
                if (!m_directory_queue_size)
                {
                    auto val = (size_t)std::count(std::begin(m_can_stop), std::end(m_can_stop), 1);
                    if (val == m_can_stop.size())
                    {
                        m_should_work = 0;
                        m_result_mutex.lock();
                        if (m_result)
                        {
                            m_result->m_ready = 1;
                            m_result->m_path = std::string();
                            m_result = nullptr;
                        }
                        m_result_mutex.unlock();
                    }
                }
            }
        }
    }

    void DirectorySearcher::process_dir(const std::string &path)
    {
        namespace fs = std::filesystem;

        std::string dir_name;
        std::string dir_path;
        std::error_code code;

        if (fs::exists(path, code))
        {
            if (code != code.default_error_condition())
            {
                return;
            }

            for (fs::directory_iterator it(path, fs::directory_options::skip_permission_denied, code);
                 it != fs::end(it);
                 code.clear(), it.increment(code))
            {
                if (code != code.default_error_condition())
                {
                    break;
                }
                if (it->is_directory(code))
                {
                    if (code != code.default_error_condition())
                    {
                        continue;
                    }

                    dir_name = it->path().filename().string();
                    dir_path = it->path().string();

                    if (dir_name == m_directory_to_search)
                    {
                        m_should_work = 0;
                        m_result_mutex.lock();
                        if (m_result)
                        {
                            m_result->m_ready = 1;
                            m_result->m_path = dir_path;
                            m_result = nullptr;
                        }
                        m_result_mutex.unlock();
                        break;
                    }
                    else
                    {
                        m_queue_mutex.lock();
                        m_directory_queue.push(dir_path);
                        m_directory_queue_size++;
                        m_queue_mutex.unlock();
                    }
                }
            }
        }
    }

}
